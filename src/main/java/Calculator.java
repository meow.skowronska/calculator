public class Calculator {
    public int add(int a,int b){
        return a+b;
    }
    public int substract(int a,int b){
        return a-b;
    }
    public int multiply(int a,int b){
        return a*b;
    }
    public double divide(int a,int b){
        if(b==0){
            System.out.println("dzielenie przez zero");
            return 0;
        }else{
            return (double)a/b;
        }

    }
    public double power(double a ,double b){
        return Math.pow(a,b);
    }
}
